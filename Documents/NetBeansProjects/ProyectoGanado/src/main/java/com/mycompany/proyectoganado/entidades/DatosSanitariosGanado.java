/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectoganado.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dahlia
 */
@Entity
@Table(name = "datos_sanitarios_ganado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosSanitariosGanado.findAll", query = "SELECT d FROM DatosSanitariosGanado d"),
    @NamedQuery(name = "DatosSanitariosGanado.findByNumsanitario", query = "SELECT d FROM DatosSanitariosGanado d WHERE d.numsanitario = :numsanitario"),
    @NamedQuery(name = "DatosSanitariosGanado.findByAseosanitario", query = "SELECT d FROM DatosSanitariosGanado d WHERE d.aseosanitario = :aseosanitario"),
    @NamedQuery(name = "DatosSanitariosGanado.findByVacunassanitario", query = "SELECT d FROM DatosSanitariosGanado d WHERE d.vacunassanitario = :vacunassanitario"),
    @NamedQuery(name = "DatosSanitariosGanado.findByAlimentacionsanitario", query = "SELECT d FROM DatosSanitariosGanado d WHERE d.alimentacionsanitario = :alimentacionsanitario"),
    @NamedQuery(name = "DatosSanitariosGanado.findByMedicosanitario", query = "SELECT d FROM DatosSanitariosGanado d WHERE d.medicosanitario = :medicosanitario")})
public class DatosSanitariosGanado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Num_sanitario")
    private Integer numsanitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Aseo_sanitario")
    @Temporal(TemporalType.DATE)
    private Date aseosanitario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Vacunas_sanitario")
    private String vacunassanitario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Alimentacion_sanitario")
    @Temporal(TemporalType.DATE)
    private Date alimentacionsanitario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Medico_sanitario")
    private String medicosanitario;
    @JoinColumn(name = "Num_ganado", referencedColumnName = "Num_ganado")
    @ManyToOne(optional = false)
    private DatosPersonalesGanado numganado;

    public DatosSanitariosGanado() {
    }

    public DatosSanitariosGanado(Integer numsanitario) {
        this.numsanitario = numsanitario;
    }

    public DatosSanitariosGanado(Integer numsanitario, Date aseosanitario, String vacunassanitario, Date alimentacionsanitario, String medicosanitario) {
        this.numsanitario = numsanitario;
        this.aseosanitario = aseosanitario;
        this.vacunassanitario = vacunassanitario;
        this.alimentacionsanitario = alimentacionsanitario;
        this.medicosanitario = medicosanitario;
    }

    public Integer getNumsanitario() {
        return numsanitario;
    }

    public void setNumsanitario(Integer numsanitario) {
        this.numsanitario = numsanitario;
    }

    public Date getAseosanitario() {
        return aseosanitario;
    }

    public void setAseosanitario(Date aseosanitario) {
        this.aseosanitario = aseosanitario;
    }

    public String getVacunassanitario() {
        return vacunassanitario;
    }

    public void setVacunassanitario(String vacunassanitario) {
        this.vacunassanitario = vacunassanitario;
    }

    public Date getAlimentacionsanitario() {
        return alimentacionsanitario;
    }

    public void setAlimentacionsanitario(Date alimentacionsanitario) {
        this.alimentacionsanitario = alimentacionsanitario;
    }

    public String getMedicosanitario() {
        return medicosanitario;
    }

    public void setMedicosanitario(String medicosanitario) {
        this.medicosanitario = medicosanitario;
    }

    public DatosPersonalesGanado getNumganado() {
        return numganado;
    }

    public void setNumganado(DatosPersonalesGanado numganado) {
        this.numganado = numganado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numsanitario != null ? numsanitario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosSanitariosGanado)) {
            return false;
        }
        DatosSanitariosGanado other = (DatosSanitariosGanado) object;
        if ((this.numsanitario == null && other.numsanitario != null) || (this.numsanitario != null && !this.numsanitario.equals(other.numsanitario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.proyectoganado.entidades.DatosSanitariosGanado[ numsanitario=" + numsanitario + " ]";
    }
    
}
