/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyecto.controladores;

import com.mycompany.proyecto.controladores.exceptions.IllegalOrphanException;
import com.mycompany.proyecto.controladores.exceptions.NonexistentEntityException;
import com.mycompany.proyecto.controladores.exceptions.PreexistingEntityException;
import com.mycompany.proyecto.controladores.exceptions.RollbackFailureException;
import com.mycompany.proyectoganado.entidades.DatosPersonalesGanado;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.mycompany.proyectoganado.entidades.DatosSanitariosGanado;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Dahlia
 */
public class DatosPersonalesGanadoJpaController implements Serializable {

    public DatosPersonalesGanadoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DatosPersonalesGanado datosPersonalesGanado) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (datosPersonalesGanado.getDatosSanitariosGanadoList() == null) {
            datosPersonalesGanado.setDatosSanitariosGanadoList(new ArrayList<DatosSanitariosGanado>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<DatosSanitariosGanado> attachedDatosSanitariosGanadoList = new ArrayList<DatosSanitariosGanado>();
            for (DatosSanitariosGanado datosSanitariosGanadoListDatosSanitariosGanadoToAttach : datosPersonalesGanado.getDatosSanitariosGanadoList()) {
                datosSanitariosGanadoListDatosSanitariosGanadoToAttach = em.getReference(datosSanitariosGanadoListDatosSanitariosGanadoToAttach.getClass(), datosSanitariosGanadoListDatosSanitariosGanadoToAttach.getNumsanitario());
                attachedDatosSanitariosGanadoList.add(datosSanitariosGanadoListDatosSanitariosGanadoToAttach);
            }
            datosPersonalesGanado.setDatosSanitariosGanadoList(attachedDatosSanitariosGanadoList);
            em.persist(datosPersonalesGanado);
            for (DatosSanitariosGanado datosSanitariosGanadoListDatosSanitariosGanado : datosPersonalesGanado.getDatosSanitariosGanadoList()) {
                DatosPersonalesGanado oldNumganadoOfDatosSanitariosGanadoListDatosSanitariosGanado = datosSanitariosGanadoListDatosSanitariosGanado.getNumganado();
                datosSanitariosGanadoListDatosSanitariosGanado.setNumganado(datosPersonalesGanado);
                datosSanitariosGanadoListDatosSanitariosGanado = em.merge(datosSanitariosGanadoListDatosSanitariosGanado);
                if (oldNumganadoOfDatosSanitariosGanadoListDatosSanitariosGanado != null) {
                    oldNumganadoOfDatosSanitariosGanadoListDatosSanitariosGanado.getDatosSanitariosGanadoList().remove(datosSanitariosGanadoListDatosSanitariosGanado);
                    oldNumganadoOfDatosSanitariosGanadoListDatosSanitariosGanado = em.merge(oldNumganadoOfDatosSanitariosGanadoListDatosSanitariosGanado);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findDatosPersonalesGanado(datosPersonalesGanado.getNumganado()) != null) {
                throw new PreexistingEntityException("DatosPersonalesGanado " + datosPersonalesGanado + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DatosPersonalesGanado datosPersonalesGanado) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DatosPersonalesGanado persistentDatosPersonalesGanado = em.find(DatosPersonalesGanado.class, datosPersonalesGanado.getNumganado());
            List<DatosSanitariosGanado> datosSanitariosGanadoListOld = persistentDatosPersonalesGanado.getDatosSanitariosGanadoList();
            List<DatosSanitariosGanado> datosSanitariosGanadoListNew = datosPersonalesGanado.getDatosSanitariosGanadoList();
            List<String> illegalOrphanMessages = null;
            for (DatosSanitariosGanado datosSanitariosGanadoListOldDatosSanitariosGanado : datosSanitariosGanadoListOld) {
                if (!datosSanitariosGanadoListNew.contains(datosSanitariosGanadoListOldDatosSanitariosGanado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DatosSanitariosGanado " + datosSanitariosGanadoListOldDatosSanitariosGanado + " since its numganado field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<DatosSanitariosGanado> attachedDatosSanitariosGanadoListNew = new ArrayList<DatosSanitariosGanado>();
            for (DatosSanitariosGanado datosSanitariosGanadoListNewDatosSanitariosGanadoToAttach : datosSanitariosGanadoListNew) {
                datosSanitariosGanadoListNewDatosSanitariosGanadoToAttach = em.getReference(datosSanitariosGanadoListNewDatosSanitariosGanadoToAttach.getClass(), datosSanitariosGanadoListNewDatosSanitariosGanadoToAttach.getNumsanitario());
                attachedDatosSanitariosGanadoListNew.add(datosSanitariosGanadoListNewDatosSanitariosGanadoToAttach);
            }
            datosSanitariosGanadoListNew = attachedDatosSanitariosGanadoListNew;
            datosPersonalesGanado.setDatosSanitariosGanadoList(datosSanitariosGanadoListNew);
            datosPersonalesGanado = em.merge(datosPersonalesGanado);
            for (DatosSanitariosGanado datosSanitariosGanadoListNewDatosSanitariosGanado : datosSanitariosGanadoListNew) {
                if (!datosSanitariosGanadoListOld.contains(datosSanitariosGanadoListNewDatosSanitariosGanado)) {
                    DatosPersonalesGanado oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado = datosSanitariosGanadoListNewDatosSanitariosGanado.getNumganado();
                    datosSanitariosGanadoListNewDatosSanitariosGanado.setNumganado(datosPersonalesGanado);
                    datosSanitariosGanadoListNewDatosSanitariosGanado = em.merge(datosSanitariosGanadoListNewDatosSanitariosGanado);
                    if (oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado != null && !oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado.equals(datosPersonalesGanado)) {
                        oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado.getDatosSanitariosGanadoList().remove(datosSanitariosGanadoListNewDatosSanitariosGanado);
                        oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado = em.merge(oldNumganadoOfDatosSanitariosGanadoListNewDatosSanitariosGanado);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = datosPersonalesGanado.getNumganado();
                if (findDatosPersonalesGanado(id) == null) {
                    throw new NonexistentEntityException("The datosPersonalesGanado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DatosPersonalesGanado datosPersonalesGanado;
            try {
                datosPersonalesGanado = em.getReference(DatosPersonalesGanado.class, id);
                datosPersonalesGanado.getNumganado();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The datosPersonalesGanado with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DatosSanitariosGanado> datosSanitariosGanadoListOrphanCheck = datosPersonalesGanado.getDatosSanitariosGanadoList();
            for (DatosSanitariosGanado datosSanitariosGanadoListOrphanCheckDatosSanitariosGanado : datosSanitariosGanadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DatosPersonalesGanado (" + datosPersonalesGanado + ") cannot be destroyed since the DatosSanitariosGanado " + datosSanitariosGanadoListOrphanCheckDatosSanitariosGanado + " in its datosSanitariosGanadoList field has a non-nullable numganado field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(datosPersonalesGanado);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DatosPersonalesGanado> findDatosPersonalesGanadoEntities() {
        return findDatosPersonalesGanadoEntities(true, -1, -1);
    }

    public List<DatosPersonalesGanado> findDatosPersonalesGanadoEntities(int maxResults, int firstResult) {
        return findDatosPersonalesGanadoEntities(false, maxResults, firstResult);
    }

    private List<DatosPersonalesGanado> findDatosPersonalesGanadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DatosPersonalesGanado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DatosPersonalesGanado findDatosPersonalesGanado(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DatosPersonalesGanado.class, id);
        } finally {
            em.close();
        }
    }

    public int getDatosPersonalesGanadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DatosPersonalesGanado> rt = cq.from(DatosPersonalesGanado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
