/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectoganado.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dahlia
 */
@Entity
@Table(name = "datos_personales_ganado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosPersonalesGanado.findAll", query = "SELECT d FROM DatosPersonalesGanado d"),
    @NamedQuery(name = "DatosPersonalesGanado.findByNumganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.numganado = :numganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findByOrigenganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.origenganado = :origenganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findByEdadganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.edadganado = :edadganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findBySexoganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.sexoganado = :sexoganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findByPesoganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.pesoganado = :pesoganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findByFechanacimientoganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.fechanacimientoganado = :fechanacimientoganado"),
    @NamedQuery(name = "DatosPersonalesGanado.findByFechamuerteganado", query = "SELECT d FROM DatosPersonalesGanado d WHERE d.fechamuerteganado = :fechamuerteganado")})
public class DatosPersonalesGanado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Num_ganado")
    private Integer numganado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Origen_ganado")
    private String origenganado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "Edad_ganado")
    private String edadganado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Sexo_ganado")
    private String sexoganado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Peso_ganado")
    private float pesoganado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_nacimiento_ganado")
    @Temporal(TemporalType.DATE)
    private Date fechanacimientoganado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_muerte_ganado")
    @Temporal(TemporalType.DATE)
    private Date fechamuerteganado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numganado")
    private List<DatosSanitariosGanado> datosSanitariosGanadoList;

    public DatosPersonalesGanado() {
    }

    public DatosPersonalesGanado(Integer numganado) {
        this.numganado = numganado;
    }

    public DatosPersonalesGanado(Integer numganado, String origenganado, String edadganado, String sexoganado, float pesoganado, Date fechanacimientoganado, Date fechamuerteganado) {
        this.numganado = numganado;
        this.origenganado = origenganado;
        this.edadganado = edadganado;
        this.sexoganado = sexoganado;
        this.pesoganado = pesoganado;
        this.fechanacimientoganado = fechanacimientoganado;
        this.fechamuerteganado = fechamuerteganado;
    }

    public Integer getNumganado() {
        return numganado;
    }

    public void setNumganado(Integer numganado) {
        this.numganado = numganado;
    }

    public String getOrigenganado() {
        return origenganado;
    }

    public void setOrigenganado(String origenganado) {
        this.origenganado = origenganado;
    }

    public String getEdadganado() {
        return edadganado;
    }

    public void setEdadganado(String edadganado) {
        this.edadganado = edadganado;
    }

    public String getSexoganado() {
        return sexoganado;
    }

    public void setSexoganado(String sexoganado) {
        this.sexoganado = sexoganado;
    }

    public float getPesoganado() {
        return pesoganado;
    }

    public void setPesoganado(float pesoganado) {
        this.pesoganado = pesoganado;
    }

    public Date getFechanacimientoganado() {
        return fechanacimientoganado;
    }

    public void setFechanacimientoganado(Date fechanacimientoganado) {
        this.fechanacimientoganado = fechanacimientoganado;
    }

    public Date getFechamuerteganado() {
        return fechamuerteganado;
    }

    public void setFechamuerteganado(Date fechamuerteganado) {
        this.fechamuerteganado = fechamuerteganado;
    }

    @XmlTransient
    public List<DatosSanitariosGanado> getDatosSanitariosGanadoList() {
        return datosSanitariosGanadoList;
    }

    public void setDatosSanitariosGanadoList(List<DatosSanitariosGanado> datosSanitariosGanadoList) {
        this.datosSanitariosGanadoList = datosSanitariosGanadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numganado != null ? numganado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosPersonalesGanado)) {
            return false;
        }
        DatosPersonalesGanado other = (DatosPersonalesGanado) object;
        if ((this.numganado == null && other.numganado != null) || (this.numganado != null && !this.numganado.equals(other.numganado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.proyectoganado.entidades.DatosPersonalesGanado[ numganado=" + numganado + " ]";
    }
    
}
