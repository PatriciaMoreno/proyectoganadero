/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectoganado.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dahlia
 */
@Entity
@Table(name = "eventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventos_1.findAll", query = "SELECT e FROM Eventos_1 e"),
    @NamedQuery(name = "Eventos_1.findByNumevento", query = "SELECT e FROM Eventos_1 e WHERE e.numevento = :numevento"),
    @NamedQuery(name = "Eventos_1.findByNumganado", query = "SELECT e FROM Eventos_1 e WHERE e.numganado = :numganado"),
    @NamedQuery(name = "Eventos_1.findByVacunasevento", query = "SELECT e FROM Eventos_1 e WHERE e.vacunasevento = :vacunasevento"),
    @NamedQuery(name = "Eventos_1.findByConsultaveterinarioevento", query = "SELECT e FROM Eventos_1 e WHERE e.consultaveterinarioevento = :consultaveterinarioevento")})
public class Eventos_1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Num_evento")
    private Integer numevento;
    @Column(name = "Num_ganado")
    private Integer numganado;
    @Size(max = 60)
    @Column(name = "Vacunas_evento")
    private String vacunasevento;
    @Column(name = "Consulta_veterinario_evento")
    @Temporal(TemporalType.DATE)
    private Date consultaveterinarioevento;

    public Eventos_1() {
    }

    public Eventos_1(Integer numevento) {
        this.numevento = numevento;
    }

    public Integer getNumevento() {
        return numevento;
    }

    public void setNumevento(Integer numevento) {
        this.numevento = numevento;
    }

    public Integer getNumganado() {
        return numganado;
    }

    public void setNumganado(Integer numganado) {
        this.numganado = numganado;
    }

    public String getVacunasevento() {
        return vacunasevento;
    }

    public void setVacunasevento(String vacunasevento) {
        this.vacunasevento = vacunasevento;
    }

    public Date getConsultaveterinarioevento() {
        return consultaveterinarioevento;
    }

    public void setConsultaveterinarioevento(Date consultaveterinarioevento) {
        this.consultaveterinarioevento = consultaveterinarioevento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numevento != null ? numevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventos_1)) {
            return false;
        }
        Eventos_1 other = (Eventos_1) object;
        if ((this.numevento == null && other.numevento != null) || (this.numevento != null && !this.numevento.equals(other.numevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.proyectoganado.entidades.Eventos_1[ numevento=" + numevento + " ]";
    }
    
}
