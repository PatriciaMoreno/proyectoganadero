/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyectoganado.entidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

/**
 *
 * @author Dahlia
 */
public class Conexion {
    
    protected Connection con;
    protected Statement stmt;
    private String serverName= "localhost";
    private String portName= "3306";
    public String db = "mivaquita";
    public String url = "jdbc:mysql://localhost:3306/" + db;
    public String user = "root";
    public String pass = "";

    private String errString;
    
    public Conexion() {

    }
    
    private String getConnectionUrl(){
        return url;
    }
    
    

    public Connection conectar() {
        con = null;

        try {
            Class.forName("org.gjt.mm.mysql.Driver");
            con = DriverManager.getConnection(getConnectionUrl(), this.user, this.pass);
            stmt = con.createStatement();
            System.out.println("Conectado");
        } catch (Exception e) {
            errString= "Error Mientras Se Conecta A La Base De Datos";
            System.out.println(errString);
            
        }

        return con;
    }
    
    public void Desconectar(){
        
        try{
            stmt.close();
            con.close();
        }catch (SQLException e){
            errString="Error Mientras Se Cerraba la Conexion a la Base de Datos";
        }
    }
    
    public Statement getStmt(){
        return this.stmt;
    }

}