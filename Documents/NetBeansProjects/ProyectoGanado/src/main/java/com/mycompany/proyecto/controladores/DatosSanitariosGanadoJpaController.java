/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proyecto.controladores;

import com.mycompany.proyecto.controladores.exceptions.NonexistentEntityException;
import com.mycompany.proyecto.controladores.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.mycompany.proyectoganado.entidades.DatosPersonalesGanado;
import com.mycompany.proyectoganado.entidades.DatosSanitariosGanado;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Dahlia
 */
public class DatosSanitariosGanadoJpaController implements Serializable {

    public DatosSanitariosGanadoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DatosSanitariosGanado datosSanitariosGanado) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DatosPersonalesGanado numganado = datosSanitariosGanado.getNumganado();
            if (numganado != null) {
                numganado = em.getReference(numganado.getClass(), numganado.getNumganado());
                datosSanitariosGanado.setNumganado(numganado);
            }
            em.persist(datosSanitariosGanado);
            if (numganado != null) {
                numganado.getDatosSanitariosGanadoList().add(datosSanitariosGanado);
                numganado = em.merge(numganado);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DatosSanitariosGanado datosSanitariosGanado) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DatosSanitariosGanado persistentDatosSanitariosGanado = em.find(DatosSanitariosGanado.class, datosSanitariosGanado.getNumsanitario());
            DatosPersonalesGanado numganadoOld = persistentDatosSanitariosGanado.getNumganado();
            DatosPersonalesGanado numganadoNew = datosSanitariosGanado.getNumganado();
            if (numganadoNew != null) {
                numganadoNew = em.getReference(numganadoNew.getClass(), numganadoNew.getNumganado());
                datosSanitariosGanado.setNumganado(numganadoNew);
            }
            datosSanitariosGanado = em.merge(datosSanitariosGanado);
            if (numganadoOld != null && !numganadoOld.equals(numganadoNew)) {
                numganadoOld.getDatosSanitariosGanadoList().remove(datosSanitariosGanado);
                numganadoOld = em.merge(numganadoOld);
            }
            if (numganadoNew != null && !numganadoNew.equals(numganadoOld)) {
                numganadoNew.getDatosSanitariosGanadoList().add(datosSanitariosGanado);
                numganadoNew = em.merge(numganadoNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = datosSanitariosGanado.getNumsanitario();
                if (findDatosSanitariosGanado(id) == null) {
                    throw new NonexistentEntityException("The datosSanitariosGanado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DatosSanitariosGanado datosSanitariosGanado;
            try {
                datosSanitariosGanado = em.getReference(DatosSanitariosGanado.class, id);
                datosSanitariosGanado.getNumsanitario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The datosSanitariosGanado with id " + id + " no longer exists.", enfe);
            }
            DatosPersonalesGanado numganado = datosSanitariosGanado.getNumganado();
            if (numganado != null) {
                numganado.getDatosSanitariosGanadoList().remove(datosSanitariosGanado);
                numganado = em.merge(numganado);
            }
            em.remove(datosSanitariosGanado);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DatosSanitariosGanado> findDatosSanitariosGanadoEntities() {
        return findDatosSanitariosGanadoEntities(true, -1, -1);
    }

    public List<DatosSanitariosGanado> findDatosSanitariosGanadoEntities(int maxResults, int firstResult) {
        return findDatosSanitariosGanadoEntities(false, maxResults, firstResult);
    }

    private List<DatosSanitariosGanado> findDatosSanitariosGanadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DatosSanitariosGanado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DatosSanitariosGanado findDatosSanitariosGanado(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DatosSanitariosGanado.class, id);
        } finally {
            em.close();
        }
    }

    public int getDatosSanitariosGanadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DatosSanitariosGanado> rt = cq.from(DatosSanitariosGanado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
